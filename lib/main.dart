import 'package:basic_widget/column.dart';
import 'package:basic_widget/list_view.dart';
import 'package:basic_widget/list_view_builder.dart';
import 'package:basic_widget/list_view_separated.dart';
import 'package:basic_widget/row.dart';
import 'package:basic_widget/single_child_layout/align.dart';
import 'package:basic_widget/single_child_layout/center.dart';
import 'package:basic_widget/single_child_layout/container.dart';
import 'package:basic_widget/icon.dart';
import 'package:basic_widget/single_child_layout/padding.dart';
import 'package:basic_widget/stack.dart';
import 'package:basic_widget/text.dart';
import 'package:basic_widget/image.dart';
import 'package:basic_widget/text_button.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Basic Widget'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          children: [
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const TextPage()),
                  );
                },
                child: const Text(
                  'Text',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ButtonPage()),
                  );
                },
                child: const Text(
                  'Button',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const IconPage()),
                  );
                },
                child: const Text(
                  'Icon',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ImagePage()),
                  );
                },
                child: const Text(
                  'Image',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ContainerPage()),
                  );
                },
                child: const Text(
                  'Container',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const CenterPage()),
                  );
                },
                child: const Text(
                  'Center',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AlignPage()),
                  );
                },
                child: const Text(
                  'Align',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PaddingPage()),
                  );
                },
                child: const Text(
                  'Padding',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ColumnWidget()),
                  );
                },
                child: const Text(
                  'Column',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const RowWidget()),
                  );
                },
                child: const Text(
                  'Row',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ListViewWidget()),
                  );
                },
                child: const Text(
                  'ListView',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ListViewBuilderWidget()),
                  );
                },
                child: const Text(
                  'ListView.builder',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ListViewSeparatedWidget()),
                  );
                },
                child: const Text(
                  'ListView.separated',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const StackPage()),
                  );
                },
                child: const Text(
                  'Stack',
                  style: TextStyle(fontSize: 20),
                )),
          ],
        ),
      ),
    );
  }
}
