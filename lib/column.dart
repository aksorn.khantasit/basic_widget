import 'package:flutter/material.dart';

class ColumnWidget extends StatelessWidget {
  const ColumnWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Column'),
      ),
      body: Column(
        children:const [
          Icon(Icons.icecream_outlined, size: 48,),
          Icon(Icons.access_time_sharp, size: 48,),
          Icon(Icons.air_sharp, size: 48,),
          Icon(Icons.accessible, size: 48,)
        ],
      ),
    );
  }
}
