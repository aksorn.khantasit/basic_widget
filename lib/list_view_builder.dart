import 'package:flutter/material.dart';

class ListViewBuilderWidget extends StatelessWidget {
  const ListViewBuilderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ListView.builder'),
      ),
      body: ListView.builder(
        itemCount: 100,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text('index $index'),
            leading: const Icon(
              Icons.icecream_outlined,
              size: 30.0,
            ),
            trailing: const Text('Click!'),
          );
        },
        padding: const EdgeInsets.all(20.0),
      ),
    );
  }
}
