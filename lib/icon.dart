import 'package:flutter/material.dart';

class IconPage extends StatelessWidget {
  const IconPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ICON PAGE'),
      ),
      body: GridView.count(
        crossAxisCount: 5,
        children: const [
          Icon(Icons.ten_k),
          Icon(Icons.ac_unit),
          Icon(Icons.access_alarm),
          Icon(Icons.access_time),
          Icon(Icons.accessibility),
          Icon(Icons.accessible),
          Icon(Icons.account_balance),
          Icon(Icons.account_balance_wallet_outlined),
          Icon(Icons.account_box),
          Icon(Icons.account_circle),
          Icon(Icons.account_tree_outlined),
          Icon(Icons.ad_units),
          Icon(Icons.adb),
          Icon(Icons.add),
          Icon(Icons.add_a_photo_outlined),
          Icon(Icons.add_alarm),
          Icon(Icons.add_alert),
          Icon(Icons.add_call),
          Icon(Icons.add_shopping_cart),
          Icon(Icons.add_location_alt_outlined),
          Icon(Icons.add_photo_alternate_outlined),
          Icon(Icons.add_to_drive),
          Icon(Icons.add_to_photos),
          Icon(Icons.addchart),
          Icon(Icons.arrow_downward),
          Icon(Icons.architecture),
          Icon(Icons.archive),
          Icon(Icons.arrow_back),
          Icon(Icons.arrow_back_ios_sharp),
          Icon(Icons.arrow_circle_up),
          Icon(Icons.baby_changing_station),
          Icon(Icons.backpack),
          Icon(Icons.backspace_outlined),
          Icon(Icons.backup),
          Icon(Icons.bakery_dining),
          Icon(Icons.cake),
          Icon(Icons.cancel),
          Icon(Icons.code),
          Icon(Icons.cached),
          Icon(Icons.calculate_outlined),
          Icon(Icons.description),
          Icon(Icons.dangerous),
          Icon(Icons.dashboard),
          Icon(Icons.data_usage),
          Icon(Icons.date_range),
        ],
      ),
    );
  }
}
