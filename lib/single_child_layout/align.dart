import 'package:flutter/material.dart';

class AlignPage extends StatelessWidget {
  const AlignPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Align'),
        ),
        body: const Align(
            alignment: Alignment.topCenter,
            child: Text(
              'Hello world',
              style: TextStyle(fontSize: 20),
            )));
  }
}
