import 'package:flutter/material.dart';

class PaddingPage extends StatelessWidget {
  const PaddingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: const Color(0xFFFFF59D),
        appBar: AppBar(
          title: const Text('Padding'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          // padding: const EdgeInsets.fromLTRB(12, 20, 12, 12),
          child: Container(
            color: Colors.pink,
            height: 100,
            width: 100,
          ),
        )
    );
  }
}
