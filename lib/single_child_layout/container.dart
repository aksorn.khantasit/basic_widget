import 'package:flutter/material.dart';

class ContainerPage extends StatelessWidget {
  const ContainerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFFF59D),
        appBar: AppBar(
          title: const Text('Container'),
        ),
        body: Container(
            margin: const EdgeInsets.all(32),
            decoration:
            BoxDecoration(
              color: const Color(0xFFF9A825),
              border: Border.all(width: 8),
            ),
            padding: const EdgeInsets.all(32),
            height: 200,
            width: 200,
            child: Container(
              color: const Color(0xFFF57F17),
              height: 100,
              width: 100,
            )
        )
    );
  }
}
