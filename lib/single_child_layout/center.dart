import 'package:flutter/material.dart';

class CenterPage extends StatelessWidget {
  const CenterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: const Text('Center'),
        ),
        body: const Center(
            child: Text('Hello world', style: TextStyle(fontSize: 20),)
        )
    );
  }
}
