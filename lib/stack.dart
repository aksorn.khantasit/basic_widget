import 'package:flutter/material.dart';

class StackPage extends StatelessWidget {
  const StackPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Stack'),
        ),
        body: Stack(
          // clipBehavior: Clip.none,
          // fit: StackFit.expand,
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              width: 300,
              height: 300,
              color: Colors.red,
            ),
            Container(
              width: 200,
              height: 200,
              color: Colors.amber,
            ),
            Positioned(
              bottom: -50,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.black,
              ),
            ),
          ],
        )
    );
  }
}
