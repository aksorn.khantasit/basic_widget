import 'package:flutter/material.dart';

class ButtonPage extends StatelessWidget {
  const ButtonPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('BUTTON'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              children: [
                _textButton(),
              ],
            ),
          ),
        ));
  }

  _textButton() {
    return Column(
      children: [
        const Text('TextButton', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
        const SizedBox(height: 20),
        TextButton(
          style: TextButton.styleFrom(
            textStyle: const TextStyle(fontSize: 20),
          ),
          onPressed: null,
          child: const Text('Disabled'),
        ),
        const SizedBox(height: 30),
        TextButton(
          style: TextButton.styleFrom(
            textStyle: const TextStyle(fontSize: 20),
          ),
          onPressed: () {},
          child: const Text('Enabled'),
        ),
        const SizedBox(height: 30),
        Container(
          decoration: const BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: TextButton(
            style: TextButton.styleFrom(
              padding: const EdgeInsets.all(16.0),
              primary: Colors.red,
              textStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            onPressed: () {},
            child: const Text('Click here!'),
          ),
        ),
      ],
    );
  }

  _elevatedButton() {
    return Column(
      children: [
        const Divider(
          color: Colors.black,
        ),
        const SizedBox(height: 30),
        const Text(
          'ElevatedButton',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 20),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 20)),
          onPressed: null,
          child: const Text('Disabled'),
        ),
        const SizedBox(height: 30),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 20)),
          onPressed: () {},
          child: const Text('Enabled'),
        ),
      ],
    );
  }

  _outlinedButton() {
    return Column(children: [
      const SizedBox(height: 30),
      const Divider(color: Colors.black,),
      const SizedBox(height: 30),
      const Text('OutlinedButton', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
      const SizedBox(height: 30),
      OutlinedButton(
        onPressed: () {
          debugPrint('Received click');
        },
        child: const Text('Click Me'),
      )
    ],);

  }
}
