import 'package:flutter/material.dart';

class TextPage extends StatelessWidget {
  const TextPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: const Text('Text'),
        ),
        body: const Center(
            child: Text('Hello world', style: TextStyle(fontSize: 20),)
        )
    );
  }
}
