import 'package:flutter/material.dart';

class ListViewSeparatedWidget extends StatelessWidget {
  const ListViewSeparatedWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ListView.separated'),
      ),
      body: ListView.separated(
          itemBuilder: (BuildContext context, int index){
            return Container(
              padding: const EdgeInsets.all(20),
              child: Text('index $index'),
              color: Colors.orange,
            );
          },
          separatorBuilder: (BuildContext context, int index){
            return const Divider(
              color: Colors.black,
              thickness: 3,
            );
          },
          itemCount: 100),
    );
  }
}
