
import 'package:flutter/material.dart';

class ImagePage extends StatelessWidget {
  const ImagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('IMAGE PAGE'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children: [
            Image.asset('images/flutter_bg.jpeg'),
            Image.network('https://nextflow.in.th/wp-content/uploads/image2.png')
          ],),
        ));
  }
}
